//
//  CustomAlertViewController.swift
//  GoogleLoginDemo
//
//  Created by gray buster on 8/20/18.
//  Copyright © 2018 gray buster. All rights reserved.
//

import UIKit

protocol CustomAlertViewDelegate: class {
    func okButtonTapped()
    func cancelButtonTapped()
}

class CustomAlertViewController: UIViewController {

    @IBOutlet weak var cancelBtn: UIButton!
    @IBOutlet weak var okayBtn: UIButton!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var warningLabel: UILabel!
    @IBOutlet weak var alertView: UIView!
    
    
    var delegate: CustomAlertViewDelegate?
    
    @IBAction func onClickedBtn(_ sender: UIButton) {
        switch sender {
        case okayBtn:
            delegate?.okButtonTapped()
            self.dismiss(animated: true, completion: nil)
        case cancelBtn:
            delegate?.cancelButtonTapped()
            self.dismiss(animated: true, completion: nil)
        default:
            break
        }
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        animateView()
    }

    func setupView() {
        alertView.layer.cornerRadius = 15
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.4)
    }
    
    func animateView() {
        alertView.alpha = 0;
        self.alertView.frame.origin.y = self.alertView.frame.origin.y + 50
        
        
        UIView.animate(withDuration: 0.4) {
            self.alertView.alpha = 1.0
            self.alertView.frame.origin.y = self.alertView.frame.origin.y - 50
        }
    }
        

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

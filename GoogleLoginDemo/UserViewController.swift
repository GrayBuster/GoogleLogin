//
//  UserViewController.swift
//  GoogleLoginDemo
//
//  Created by gray buster on 8/16/18.
//  Copyright © 2018 gray buster. All rights reserved.
//

import UIKit
import FacebookLogin
import FBSDKCoreKit
import FBSDKLoginKit
import GoogleMaps
import GooglePlaces
import SQLite

class UserViewController: UIViewController,CustomAlertViewDelegate {
    
    
    @IBOutlet weak var contentMap: UIView!
    
    var locationManager = CLLocationManager()
    
    var mapView: GMSMapView!
    
    var placesClient: GMSPlacesClient!

    
    var zoomLevel: Float = 15.0
    
    var resultsViewController: GMSAutocompleteResultsViewController?
    
    var searchController: UISearchController?
    
    var resultView: UITextView?
    
    var marker: GMSMarker?
    
    var states = [State]()
    
    var myLocation: CLLocation? {
        didSet {
            if states.count > 0 {
                states.forEach { state in
                    if self.myLocation!.coordinate.longitude != state.long && self.myLocation!.coordinate.latitude != state.lat {
                        createMarkers(with: CLLocation(latitude: state.lat, longitude: state.long), name: nil)
                    }
                }
            }
        }
    }
    
    var infoWindow:CustomInfoView = {
        return CustomInfoView.instanceFromNib() as! CustomInfoView
    }()

    //MARK: Alert Custom Delegate
    func okButtonTapped() {
        if GIDSignIn.sharedInstance().hasAuthInKeychain() {
            GIDSignIn.sharedInstance().signOut()
        }else {
            FBSDKLoginManager().logOut()
        }
        
        self.performSegue(withIdentifier: "Sign out", sender: nil)
    }
    
    func cancelButtonTapped() {
        return
    }
    
    var user : User?
    
    //Database
    var context: Connection?
    
    var geocoder = CLGeocoder()

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var usernameLabel: UILabel!
    
    @IBOutlet var spinner: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        spinner.center = imageView.center
        view.addSubview(spinner)
        configGoogleMap()
        configSearchBarAddress()
        //infoWindow.delegate = self
        guard let context = context else {
            print("context is nil")
            return
        }
        
        let table = Table("State")
        let latitude = Expression<Double>("latitude")
        let longtitude = Expression<Double>("longtitude")
        let image = Expression<String>("image")
        for state in try! context.prepare(table) {
            if !state[image].isEmpty {
                states.append(State(streetName: "", address: "", image: "\(state[image])", long: state[longtitude], lat: state[latitude]))
            }else {
                states.append(State(streetName: "", address: "", image: "", long: state[longtitude], lat: state[latitude]))
            }
            
        }
    }
    
  
    
    @IBAction func signout(_ sender: UIBarButtonItem) {
        
        //Custom alert view controller
        let storyboard = UIStoryboard(name: "AlertCustomView", bundle: nil)
        let customAlert = storyboard.instantiateViewController(withIdentifier: "CustomAlertID") as! CustomAlertViewController
        customAlert.providesPresentationContextTransitionStyle = true
        customAlert.definesPresentationContext = true
        customAlert.modalPresentationStyle = .overCurrentContext
        customAlert.modalTransitionStyle = .crossDissolve
        customAlert.delegate = self
        self.present(customAlert, animated: true, completion: nil)
    }
    
    func getDataFromUrl(url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        URLSession.shared.dataTask(with: url) { data, response, error in
            completion(data, response, error)
            }.resume()
    }
    
    //Map configuration
    func configGoogleMap() {
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.requestAlwaysAuthorization()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.distanceFilter = 50
        locationManager.startUpdatingLocation()
        placesClient = GMSPlacesClient.shared()
        
    }
    
    func createMarkers(with location: CLLocation, name: String?) {
        marker = GMSMarker()
        marker!.position = CLLocationCoordinate2D(latitude: location.coordinate.latitude , longitude: location.coordinate.longitude)
        marker!.icon = #imageLiteral(resourceName: "person-icon-1")
        marker?.infoWindowAnchor = CGPoint(x: 0.44, y: 0.45)
        if name != nil {
            marker?.title = name
        }
        marker!.map = mapView
        
    }
    
    
    //Configure Search bar controller
    func configSearchBarAddress() {
        resultsViewController = GMSAutocompleteResultsViewController()
        resultsViewController?.delegate = self
        
        searchController = UISearchController(searchResultsController: resultsViewController)
        searchController?.searchResultsUpdater = resultsViewController
        
        // Put the search bar in the navigation bar.
        searchController?.searchBar.placeholder = "Search address"
        searchController?.searchBar.sizeToFit()
        navigationItem.titleView = searchController?.searchBar
        
        // When UISearchController presents the results view, present it in
        // this view controller, not one further up the chain.
        definesPresentationContext = true
        
        // Prevent the navigation bar from being hidden when searching.
        searchController?.hidesNavigationBarDuringPresentation = false
    }
    
    
    //MARK: View controller life cycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        spinner.startAnimating()
        usernameLabel.text = ""
        emailLabel.text = ""
    }
 
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if let user = user {
            usernameLabel.text = user.username
            emailLabel.text = user.email
            
            DispatchQueue.global().async {
                if user.image != nil, let url = URL(string: user.image!) , !(user.image?.isEmpty)! {
                    self.getDataFromUrl(url: url, completion: { data,_,error in
                        if data != nil , error == nil {
                        DispatchQueue.main.async {
                                self.imageView.image = UIImage(data: data!)
                                self.spinner.stopAnimating()
                            }
                        }else {
                            DispatchQueue.main.async {
                                self.spinner.stopAnimating()
                            }
                        }
                    })
                }
                
            }
        }else {
            self.spinner.stopAnimating()
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "Show Street View" {
            if let streetVC = segue.destination as? StreetViewController,
                let popoverVC = streetVC.popoverPresentationController {
                if self.user != nil {
                    var bounds = GMSCoordinateBounds()
                    bounds = bounds.includingCoordinate(marker!.position)
                    popoverVC.delegate = self
                    
                    Timer.scheduledTimer(withTimeInterval: 1.0, repeats: false) { _ in
                        let point = self.mapView.projection.point(for: self.marker!.position)
                        popoverVC.sourceView = UIView(frame: CGRect(origin: point, size: CGSize(width: 10.0, height: 10.0)))
                        popoverVC.sourceRect = UIView(frame: CGRect(origin: point, size: CGSize(width: 10.0, height: 10.0))).bounds
                    }
                    popoverVC.permittedArrowDirections = .up
                    streetVC.state = user!.state
                }
            }else {
                print("nil")
            }
        } else if segue.identifier == "State Data" {
            
        }
    }
}

extension UserViewController: CLLocationManagerDelegate,GMSAutocompleteResultsViewControllerDelegate,GMSMapViewDelegate,GMSPanoramaViewDelegate {
    
    //MARK: Search Address Google Delegate
    func resultsController(_ resultsController: GMSAutocompleteResultsViewController, didAutocompleteWith place: GMSPlace) {
        searchController?.isActive = false
        let camera = GMSCameraPosition(target: place.coordinate, zoom: zoomLevel, bearing: 0, viewingAngle: 0)
        mapView.animate(to: camera)
        
        if !isDuplicated(states, location: place.coordinate) {
            createMarkers(with: CLLocation(latitude: place.coordinate.latitude, longitude: place.coordinate.longitude), name: nil)
        }
        
    }
    
    func isDuplicated(_ states: [State],location: CLLocationCoordinate2D) -> Bool {
        for state in states {
            if state.lat == location.latitude && state.long == location.longitude {
                return true
            }
        }
       return false
    }
    
    func resultsController(_ resultsController: GMSAutocompleteResultsViewController, didFailAutocompleteWithError error: Error) {
        print("error: \(error)")
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
    //MARK: Map View Delegate
    
    //Tapped marker
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        let position = marker.position
        self.marker = marker
        let location = CLLocation(latitude: position.latitude, longitude: position.longitude)
        self.infoWindow.removeFromSuperview()
        infoWindow.contentStreetView.subviews.forEach { view in
            view.removeFromSuperview()
        }
    
        if user != nil {
            self.infoWindow.spinner.startAnimating()
            self.infoWindow.contentStreetView.backgroundColor = .clear
            geocoder.reverseGeocodeLocation(location) { (placemarksArray, error) in
                if placemarksArray != nil, placemarksArray!.count > 0 {

                    if let placemark = placemarksArray?.first {
                        if let street = placemark.name,
                            let streetName = placemark.thoroughfare,
                            let state = placemark.addressDictionary!["SubAdministrativeArea"] as? String,
                            let province = placemark.addressDictionary!["SubLocality"] as? String {
                            DispatchQueue.main.async {
                                self.user!.state = State(streetName: street, address: streetName + " " + province + " " + state , image: "", long: position.longitude , lat: position.latitude)
                                self.infoWindow.state = self.user!.state
                                self.infoWindow.tapGesture.addTarget(self, action: #selector(UserViewController.didTapInfoView))
                                let panoView = GMSPanoramaView(frame: self.infoWindow.contentStreetView.bounds)
                                panoView.moveNearCoordinate(CLLocationCoordinate2D(latitude: (self.user?.state?.lat)!, longitude: (self.user?.state?.long)!))
                                panoView.delegate = self
                                self.infoWindow.contentStreetView.addSubview(panoView)
                                //self.performSegue(withIdentifier: "Show Street View", sender: marker)
                            }
                        }
                    }
                }
            }
        }
        infoWindow.alpha = 0.9
        infoWindow.layer.cornerRadius = 12
        infoWindow.layer.borderWidth = 2
        infoWindow.layer.borderColor = UIColor.magenta.cgColor
        

        // Offset the info window to be directly above the tapped marker
        infoWindow.center = mapView.projection.point(for: location.coordinate)
        infoWindow.center.y = infoWindow.center.y + 50
        self.view.addSubview(infoWindow)
        
        
        //contentMap.addSubview(marker.panoramaView!)
        
        return false
    }
    
    @objc func didTapInfoView(gesture: UITapGestureRecognizer) {
        infoWindow.removeFromSuperview()
    }
    
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        if (marker != nil){
            guard let location = marker?.position else {
                print("locationMarker is nil")
                return
            }
            infoWindow.center = mapView.projection.point(for: location)
            infoWindow.center.y = infoWindow.center.y + 50
        }
    }

    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        self.infoWindow.removeFromSuperview()
        createMarkers(with: CLLocation(latitude: coordinate.latitude, longitude: coordinate.longitude), name: nil)
    }
    
    func didTapMyLocationButton(for mapView: GMSMapView) -> Bool {
        guard let location = myLocation else {
            print("location is nil")
            return false
        }
        let camera = GMSCameraPosition(target: location.coordinate, zoom: zoomLevel, bearing: 0, viewingAngle: 0)
        mapView.animate(to: camera)
        return true
    }
    
    func mapView(_ mapView: GMSMapView, didEndDragging marker: GMSMarker) {
        
    }
    
    //MARK: Panorama Delegate
    func panoramaView(_ view: GMSPanoramaView, didMoveTo panorama: GMSPanorama?) {
        if let pano = panorama {
            if user != nil {
                user?.state!.image = pano.panoramaID
            }
        }
    }
    
    func panoramaView(_ view: GMSPanoramaView, error: Error, onMoveNearCoordinate coordinate: CLLocationCoordinate2D) {
        infoWindow.contentStreetView.subviews.forEach { view in
            view.removeFromSuperview()
        }
        let imageView = UIImageView(frame:  infoWindow.contentStreetView.bounds)
        let image = #imageLiteral(resourceName: "no image")
        imageView.image = image
        infoWindow.contentStreetView.backgroundColor = .gray
        infoWindow.contentStreetView.addSubview(imageView)
    }
    
    //MARK: Location Manager Delegate
    // Location did update.
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.last {
            let camera = GMSCameraPosition(target: location.coordinate, zoom: zoomLevel, bearing: 0, viewingAngle: 0)
            mapView = GMSMapView.map(withFrame: contentMap.bounds, camera: camera)
            
            mapView.animate(to: camera)
            mapView.settings.myLocationButton = true
            mapView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            mapView.delegate = self
            mapView.setMinZoom(6.0, maxZoom: 30.0)
            myLocation = location
            
            // Creates a marker in the center of the map.
            marker = GMSMarker()
            marker!.position = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
            marker!.icon = #imageLiteral(resourceName: "person-icon-1")
            marker?.isDraggable = true
            marker!.infoWindowAnchor = CGPoint(x: 2.5, y: 2.5)
            //self.states.append(State(streetName: "", address: "", image: "", long: location.coordinate.longitude, lat: location.coordinate.latitude))
            
            marker!.map = mapView
            
            
            contentMap.addSubview(mapView)
            
            locationManager.stopUpdatingLocation()
        }
    }
    
    // Handle authorization for the location manager.
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .restricted:
            print("Location access was restricted.")
        case .denied:
            print("User denied access to location.")
            // Display the map using the default location.
            mapView.isHidden = false
        case .notDetermined:
            print("Location status not determined.")
        case .authorizedAlways: fallthrough
        case .authorizedWhenInUse:
            print("Location status is OK.")
        }
    }
    
    // Handle location manager errors.
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        locationManager.stopUpdatingLocation()
        print("Error: \(error)")
    }
}
//MARK: Popover Delegate
extension UserViewController: UIPopoverPresentationControllerDelegate {
    func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection: UITraitCollection) -> UIModalPresentationStyle {
        return .none
    }
}

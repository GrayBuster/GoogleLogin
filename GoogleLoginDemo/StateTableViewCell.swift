//
//  StateTableViewCell.swift
//  GoogleLoginDemo
//
//  Created by gray buster on 8/23/18.
//  Copyright © 2018 gray buster. All rights reserved.
//

import UIKit

class StateTableViewCell: UITableViewCell {
    
    var state: State?

    @IBOutlet weak var stateImageView: UIImageView!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

//
//  StreetViewController.swift
//  GoogleLoginDemo
//
//  Created by gray buster on 8/23/18.
//  Copyright © 2018 gray buster. All rights reserved.
//

import UIKit
import GoogleMaps

class StreetViewController: UIViewController,GMSPanoramaViewDelegate {
    
    @IBOutlet weak var streetNameLabel: UILabel!
    
    @IBOutlet weak var contentStreetView: UIView!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    @IBOutlet weak var longtitudeLabel: UILabel!
    @IBOutlet weak var latitudeLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    
    var state: State?

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        spinner.startAnimating()
        streetNameLabel.text = ""
        addressLabel.text = ""
        longtitudeLabel.text = ""
        latitudeLabel.text = ""
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if let state = state {
            streetNameLabel.text = state.streetName
            addressLabel.text = state.address
            longtitudeLabel.text = "\(state.long)"
            latitudeLabel.text = "\(state.lat)"
            spinner.stopAnimating()
            let panoView = GMSPanoramaView(frame: contentStreetView.bounds)
            panoView.delegate = self
            panoView.moveNearCoordinate(CLLocationCoordinate2D(latitude: state.lat, longitude: state.long))
            contentStreetView.addSubview(panoView)
        }else {
            print("nil")
        }
    }
    
    func panoramaView(_ view: GMSPanoramaView, didMoveTo panorama: GMSPanorama?) {
    }
    
    func panoramaView(_ view: GMSPanoramaView, error: Error, onMoveNearCoordinate coordinate: CLLocationCoordinate2D) {
        contentStreetView.subviews.forEach { view in
            view.removeFromSuperview()
        }
        let imageView = UIImageView(frame: contentStreetView.bounds)
        let image = #imageLiteral(resourceName: "no image")
        imageView.image = image
        contentStreetView.addSubview(imageView)
    }
    
    @IBAction func tappedDismissController(_ sender: UITapGestureRecognizer) {
        self.dismiss(animated: true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

//
//  ViewController.swift
//  GoogleLoginDemo
//
//  Created by gray buster on 8/15/18.
//  Copyright © 2018 gray buster. All rights reserved.
//

import UIKit
import SQLite
import Firebase
import FirebaseDatabase
import FacebookLogin
import FBSDKCoreKit
import FBSDKLoginKit
import GoogleMaps

protocol LoginWithGoogleDelegate: class {
    func didSucceedToSignInFor(user: User)
    func didFailToSignIn(error: Error)
}

class LoginWithGoogle: NSObject {
    
    // MARK: - Properties
    static let sharedInstance = LoginWithGoogle()
    weak var delegate: LoginWithGoogleDelegate?
    var globalViewController: UIViewController?
    
    // MARK: - Helper Methods
    /**
     configures the settings of google sign-in sdk
     - parameter viewController: it is the view controller on which we have to show the google sign-in screen
     */
    func configureGooglePlus(viewController: UIViewController) {
        globalViewController = viewController
        GIDSignIn.sharedInstance().clientID = "278506258842-78ppt1adi8vcjodgjt4gemda5v8ts741.apps.googleusercontent.com"
        GIDSignIn.sharedInstance().delegate = self
        //GIDSignIn.sharedInstance().uiDelegate = self
        //GIDSignIn.sharedInstance().signIn()
    }
}

// MARK: - GIDSignInDelegate methods
extension LoginWithGoogle: GIDSignInDelegate {
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if let error = error {
            print("\(error.localizedDescription)")
            self.delegate?.didFailToSignIn(error: error)
        } else {
            let idToken = user.authentication.idToken!
            let fullName = user.profile.name!
            let email = user.profile.email!
            //Validate id_token
            AccountUtil.instance.validate(idToken) { data,error in
                guard error == nil else { print(error!); return }
                guard let data = data else { print("dont have any data"); return }
                if let verifiedEmail = data["email"] as? String , let isVerified = data["email_verified"] as? String {
                    if verifiedEmail.contains(email) && isVerified.contains("true") {
                        if user.profile.hasImage {
                            let image = user.profile.imageURL(withDimension: 200).absoluteString
                            AccountUtil.user = User(username: fullName, email: email, token_id: idToken, image: image)
                        } else {
                            AccountUtil.user = User(username: fullName, email: email, token_id: idToken, image: nil)
                        }
                        DispatchQueue.main.async {
                            self.delegate?.didSucceedToSignInFor(user: AccountUtil.user!)
                        }
                        
                    }else {
                        print("user is not verified")
                        self.delegate?.didFailToSignIn(error: error!)
                    }
                } else {
                    print("could not convert")
                    
                }
            }
        }
    }
    
    
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
        print("Something Went Wrong")
        self.delegate?.didFailToSignIn(error: error)
    }
}


class ViewController: UIViewController,LoginWithGoogleDelegate,GIDSignInUIDelegate,LoginButtonDelegate {
    
    
    
    func loginButtonDidCompleteLogin(_ loginButton: LoginButton, result: LoginResult) {
        let request = FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, email, picture.type(large)"])
        let _ = request?.start(completionHandler: { (connection, result, error) in
            guard let userInfo = result as? [String: Any] else { print(error!); return } //handle the error
            
            
            DispatchQueue.main.async {
                AccountUtil.user = User(dict: userInfo)
                if let imageURL = ((userInfo["picture"] as? [String: Any])?["data"] as? [String: Any])?["url"] as? String {
                    //Download image from imageURL
                    AccountUtil.user?.image = imageURL
                }
                if let accessToken = FBSDKAccessToken.current() {
                    AccountUtil.user?.token_id = accessToken.tokenString!
                }
                self.performSegue(withIdentifier: "Login Successful", sender: nil)
            }
            
            //The url is nested 3 layers deep into the result so it's pretty messy
           
        })
        
        
        
//        if let _ = FBSDKAccessToken.current() {
//            if let currentUser = FBSDKProfile.current() {
//
//                print("current user got: \(currentUser.name)")
//            } else {
//                print("current user not got")
//                FBSDKProfile.loadCurrentProfile(completion: {
//                    profile, error in
//                    if let updatedUser = profile {
//
//                        if let accessToken = FBSDKAccessToken.current() {
//                            print("access token \(accessToken.tokenString!)")
//                        }
//
//                    } else {
//                        print("current user still not got")
//                    }
//                })
//            }
//        }
    }
    
    func loginButtonDidLogOut(_ loginButton: LoginButton) {
        
    }
    

    var userTable = Table("User")
    
    var stateTable = Table("State")
    
    @IBOutlet weak var facebookBtn: UIView!
    
    
    
    @IBOutlet weak var googleBtn: GIDSignInButton!
    
    var ref: DatabaseReference?
    
    func didSucceedToSignInFor(user: User) {
        let _ = Expression<Int64>("id")
        let name = Expression<String>("username")
        let email = Expression<String>("email")
        let image = Expression<String>("image")
        let token_id = Expression<String>("token_id")
        do {
            let statement = try context.run("SELECT * from User where email = ?", [user.email])
            if statement.columnCount == 0 {
                if user.image != nil {
                    let insert = userTable.insert(name <- user.username, email <- user.email, image <- user.image!, token_id <- user.token_id)
                    try context.run(insert)
                } else {
                    let insert = userTable.insert(name <- user.username, email <- user.email, image <- "", token_id <- user.token_id)
                    try context.run(insert)
                }
            }
            self.performSegue(withIdentifier: "Login Successful", sender: nil)
        }catch {
            print(error)
        }
    }
    
    @IBAction func unwind(_ segue: UIStoryboardSegue) {
        
    }
    
    func didFailToSignIn(error: Error) {
        self.showAlert(msg: "Bạn không có quyền đăng nhập!")
    }
    
    func createTable() {
        do {
            //try context.run(stateTable.delete())
            
            let id = Expression<Int64>("id")
            let name = Expression<String>("username")
            let email = Expression<String>("email")
            let image = Expression<String>("image")
            let token_id = Expression<String>("token_id")
            
            let streetName = Expression<String>("name")
            let latitude = Expression<Double>("latitude")
            let longtitude = Expression<Double>("longtitude")
            
            try context.run(stateTable.create { t in
                t.column(id,primaryKey: true)
                t.column(latitude)
                t.column(longtitude)
                t.column(image)
            })
            
            try context.run(userTable.create { t in
                t.column(id,primaryKey: true)
                t.column(name)
                t.column(email, unique: true)
                t.column(image)
                t.column(token_id)
            })
            
        }catch {
            print(error)
        }
    }
    
    func insertStateTable() {
        let streetName = Expression<String>("name")
        let latitude = Expression<Double>("latitude")
        let longtitude = Expression<Double>("longtitude")
        let image = Expression<String>("image")
        let insert = stateTable.insert(latitude <- 10.764754, longtitude <- 106.694067, image <- "")
        let insert1 = stateTable.insert(latitude <- 10.763177, longtitude <- 106.693358, image <- "https://www.google.com/maps/place/Văn+Lang+University/@10.7629738,106.6935385,3a,75y,90t/data=!3m8!1e2!3m6!1sAF1QipML9aeDHbdEe8ZTfDJEUWudmE3ETYyuUWEvM1xO!2e10!3e12!6shttps:%2F%2Flh5.googleusercontent.com%2Fp%2FAF1QipML9aeDHbdEe8ZTfDJEUWudmE3ETYyuUWEvM1xO%3Dw392-h294-k-no!7i1265!8i949!4m12!1m6!3m5!1s0x31752f16be5e11b9:0x4f7cea9c0ba8b954!2sFamilymart!8m2!3d10.7637311!4d106.6931637!3m4!1s0x31752f16ad86371b:0x949d258c9508b1f2!8m2!3d10.7629354!4d106.6933353")
        let insert2 = stateTable.insert(latitude <- 10.763795, longtitude <- 106.693152, image <- "https://www.google.com/maps/place/Familymart/@10.7637314,106.6931638,3a,75y,90t/data=!3m8!1e2!3m6!1sAF1QipPSQh0CqkGkJDKzfaEF-7RqQx4tlyqpjQ-KS4mE!2e10!3e12!6shttps:%2F%2Flh5.googleusercontent.com%2Fp%2FAF1QipPSQh0CqkGkJDKzfaEF-7RqQx4tlyqpjQ-KS4mE%3Dw392-h294-k-no!7i4128!8i3096!4m5!3m4!1s0x31752f16be5e11b9:0x4f7cea9c0ba8b954!8m2!3d10.7637311!4d106.6931637")
        let insert3 = stateTable.insert(latitude <- 10.762682, longtitude <- 106.681188, image <- "https://www.google.com/maps/place/University+of+Science,+Downtown+Campus/@10.7624165,106.6812013,3a,75y,90t/data=!3m8!1e2!3m6!1sAF1QipNDIaV0iCdQtTTzQHCHX9ecSJmDNAb4i6g59I-X!2e10!3e12!6shttps:%2F%2Flh5.googleusercontent.com%2Fp%2FAF1QipNDIaV0iCdQtTTzQHCHX9ecSJmDNAb4i6g59I-X%3Dw203-h152-k-no!7i3264!8i2448!4m5!3m4!1s0x31752f1c06f4e1dd:0x43900f1d4539a3d!8m2!3d10.7624163!4d106.681201")
        try! context.run(insert)
        try! context.run(insert1)
        try! context.run(insert2)
        try! context.run(insert3)
    }
    


    
    lazy var context: Connection = {
        let fileURL = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
            .appendingPathComponent("UserDatabase.sqlite")
        let db = try! Connection(fileURL.path)
        return db
    }()
    
    func connectFirebase() {
        ref = Database.database().reference()
        
        ref?.child("User").observeSingleEvent(of: .value, with: { (snapshot) in
            // Get user value
            if let value = snapshot.value as? [String:Any] ,let username = value["username"] as? String {
                print(username)
            }
            
        }) { (error) in
            print(error.localizedDescription)
        }
        
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        createTable()
        googleBtn.style = .standard
        let loginButton = LoginButton(readPermissions: [ .publicProfile,.email ])
        
        loginButton.center = view.center
        loginButton.delegate = self
        view.addSubview(loginButton)
        GIDSignIn.sharedInstance().uiDelegate = self
        LoginWithGoogle.sharedInstance.delegate = self
        LoginWithGoogle.sharedInstance.configureGooglePlus(viewController: self)
        //insertStateTable()
        //configGoogleMap()
        //connectFirebase()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "Login Successful" {
            if let userVC = segue.destination.content as? UserViewController {
                userVC.user = AccountUtil.user!
                userVC.context = self.context
            }
        }
    }

}

extension UIViewController {
    func showAlert(msg: String) {
        let alert = UIAlertController(title: "Cảnh báo!", message: msg, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    //Navigation Controller segues
    var content: UIViewController {
        if let navcon = self as? UINavigationController {
            return navcon.visibleViewController ?? self
        } else if let tabcon = self as? UITabBarController {
            return tabcon.selectedViewController ?? self
        } else {
            return self
        }
    }
}

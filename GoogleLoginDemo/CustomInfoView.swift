//
//  CustomInfoView.swift
//  GoogleLoginDemo
//
//  Created by gray buster on 8/23/18.
//  Copyright © 2018 gray buster. All rights reserved.
//

import UIKit


class CustomInfoView: UIView {
    
    @IBOutlet weak var streetNameLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var latitudeLabel: UILabel!
    @IBOutlet weak var longtitudeLabel: UILabel!
    @IBOutlet weak var contentStreetView: UIView!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    @IBOutlet var tapGesture: UITapGestureRecognizer!
    
    var state: State? {
        didSet {
            if let state = self.state {
                addressLabel.text = state.address
                streetNameLabel.text = state.streetName
                latitudeLabel.text = "\(state.lat)"
                longtitudeLabel.text = "\(state.long)"
                spinner.stopAnimating()
            }
        }
    }
    
    class func instanceFromNib() -> UIView {
        var view = CustomInfoView()
        let objects = Bundle.main.loadNibNamed("CustomWindowInfo", owner: self, options: nil)
        for i in objects! {
            if let myView = i as? CustomInfoView {
                view = myView
            }
        }
        return view
    }
}

//
//  File.swift
//  GoogleLoginDemo
//
//  Created by gray buster on 8/15/18.
//  Copyright © 2018 gray buster. All rights reserved.
//

import Foundation
import GoogleMaps

struct User {
    var username: String
    var email: String
    var token_id: String
    var image: String?
    var state: State?
    init(dict: [String:Any]) {
        username = dict["name"] as! String
        email = dict["email"] as! String
        image = ""
        token_id = ""
    }
    init(username: String, email: String, token_id: String, image: String?) {
        self.username = username
        self.email = email
        self.image = image
        self.token_id = token_id
    }
}

struct State {
    var streetName: String
    var address: String
    var image: String?
    var long: CLLocationDegrees
    var lat: CLLocationDegrees
}

class AccountUtil {
    
    
    static let instance = AccountUtil()
    
    //weak var delegate: LoginWithGoogleDelegate?
    
    func validate(_ token: String, completion: @escaping ([String: Any]?, Error?) -> ()) {
        if var components = URLComponents(string: "https://www.googleapis.com/oauth2/v3/tokeninfo") {
            components.queryItems = [
                URLQueryItem(name: "id_token", value: token)
            ]
            let request = URLRequest(url: components.url!)
            let task = URLSession.shared.dataTask(with: request) { data, response, error in
                guard let data = data,                            // is there data
                    let response = response as? HTTPURLResponse,  // is there HTTP response
                    (200 ..< 300) ~= response.statusCode,         // is statusCode 2XX
                    error == nil else {                           // was there no error, otherwise ...
                        completion(nil, error)
                        return
                }
                do {
                    if let responseObject = try JSONSerialization.jsonObject(with: data) as? [String: Any] {
                        completion(responseObject, nil)
                    }
                }catch {
                    completion(nil,error)
                }
            }
            task.resume()
        }
    }
    
    static var user: User?
}
